

<form action="<?=$_SERVER['REQUEST_URI']?>" method="post">
	
	<div class="form-group row justify-content-start">
		<label for="example-text-input" class="col-2 col-form-label">Заголовок</label>
		<div class="col-6">
			<input name="title" class="form-control" type="text" value="<?=$data['title']?>" id="example-text-input">
		</div>
	</div>

	<div class="form-group row justify-content-start">
		<label for="example-text-input" class="col-2 col-form-label">Описание</label>
		<div class="col-6">
			<input name="description" class="form-control" type="text" value="<?=$data['description']?>" id="example-text-input">
		</div>
	</div>
	
	<div class="form-group row justify-content-start">
		<label for="example-text-input" class="col-2 col-form-label">Ключевые слова</label>
		<div class="col-6">
			<input name="keywords" class="form-control" type="text" value="<?=$data['keywords']?>" id="example-text-input">
		</div>
	</div>

	

	<input type="submit" name="submit_seo" class="btn btn-primary" value="Изменить"/>
</form>