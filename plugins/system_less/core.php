<?php
/*
Plugin Name: LazyprogerLess
Plugin URI: http://lazyproger.ru
Description: Позволяет использовать Less
Version: 0.1
Author: Lazyproger
Author URI: http://lazyproger.ru

USE
	lessphp - https://github.com/leafo/lessphp
	csspres - https://github.com/towry/csspress.php

	Copyright 2017  lazyproger  (email: dimariolp@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	
*/
	$path = ROOTPATH.'templates/default/css/';
	$dir = scandir($path);
	$update = false;
	$globalcss = '';
	

	
	foreach($dir as $file){
		
		if($file != '.' && $file != '..' && $file != 'tmp' && $file != 'project.css'){
			
		
			
			$explode = explode('.', $file);
				
			//Проверяем формат файла
			if(!Plugins::checkformat_css($explode[1])) continue;
				
				
			$time = array(
				'style' => filemtime($path.$explode[0].'.'.$explode[1]),
				'tmp' => (is_file(($path.'tmp/'.$explode[0].'.css')) ? filemtime($path.'tmp/'.$explode[0].'.css') : 0)
			);

				
			if($time['style'] > $time['tmp']){
				$update = true;
					
				if($explode[1] == 'less'){
					$less = new lessc;
					$globalcss .= $css = $less->compileFile($path.$file);
				}else{
					$globalcss .= $css = file_get_contents($path.$file);
				}
					
				//Удаляем файл css
				
				if(is_file($path.'tmp/'.$explode[0].'.css')){
					unlink($path.'tmp/'.$explode[0].'.css');
				}
				
		
				//Пишем в файл
				file_put_contents($path.'tmp/'.$explode[0].'.css', $css);
			}else{
				$globalcss .= $css = file_get_contents($path.'tmp/'.$explode[0].'.css');
			}
		}
	}
		

	if($update){
		//Удаляем файл css
		unlink($path.'/project.css');

		$csspress = new csspress();
		$globalcss = $csspress->press($globalcss);
			
		//Пишем в файл
		file_put_contents($path.'/project.css', $globalcss);
		
	}
	
	echo '<link rel="stylesheet" href="/templates/default/css/project.css?time='.time().'">';

	

	