<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');

//Системные константы
define('ROOTPATH', dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR);
define('URL', $_SERVER['SERVER_NAME']);
define('SOLT', '@#UIO$UIOHJSDKLFHSHF&@#@()@$U@)HJIOSDFHSDFLKJHKSLDHFSOYDF');

//Доп параметры
ini_set('session.use_trans_sid', '0');
date_default_timezone_set('Asia/Yekaterinburg');

//Подключение конфига
include(ROOTPATH . 'includes/configs/core.php');

//Стартуем сессию
ini_set('session.gc_maxlifetime', 86400);
ini_set('session.save_handler', 'memcached');
ini_set('session.save_path', 'localhost:11211');
    
session_name('sid');
session_start();

// Автозагрузка Классов
spl_autoload_register('autoload');
function autoload($name)
{

	$name = strtolower($name);

 	if (class_exists($name, false)) {
 		return;
    }

	$file = ROOTPATH . 'includes/classes/' . $name . '.php';

    if (file_exists($file)) {
        require_once($file);
    }
}
