<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');

class Comments
{
	private static $instance = null;
	private static $setting = array();

	// Инициализация

	public static function getInstance($setting){

        if (is_null(self::$instance)) {
            self::$instance = new Comments;
        }

        self::$setting = $setting;
		
		if(!isset(self::$setting['refid'])) self::$setting['refid'] = 0;

		self::addComment();

        return self::$instance;
    }

	// Вывод комментариев

	public static function view() {
		$tpl = Template::getInstance();

		// Получаем число комментариев
		$tpl->commentsCount = self::countComments();
		$tpl->comments = array();
		$tpl->commentsMore = array();
		
		//Передаем настройки
		$tpl->settings = self::$setting;
		
		if($tpl->commentsCount) {
			// Получаем список комментариев
			$comments = self::getComments();
			
			foreach($comments as $comment) {
				$comment['text'] = Text::main($comment['text']);
				
				if(!$comment['refidComment']) {
					$tpl->comments[] = $comment;
					
					if(!isset($tpl->commentsMore[$comment['idcomment']])) {
						$tpl->commentsMore[$comment['idcomment']] = array();
					}					
						
				} else {
					$tpl->commentsMore[$comment['refidComment']][] = $comment;	
				}
			}
		} else {
			$tpl->comments = false;
		}
		
		// Подключаем шаблона
		$tpl->loadTpl('comments', true);

	}

	//Добавление комментария

	public static function addComment() {
		$text = isset($_POST['text']) ? trim($_POST['text']) : '';
		
		if(isset($_POST['submitAddComments']) && Vars::$USER['id'] && $text){
			$_POST['refidComment'] = (isset($_POST['refidComment']) ? ($_POST['refidComment']) : 0);
			
			if($_POST['refidComment']) {
				if(!self::isComment($_POST['refidComment'])) {
					header('location: http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
					exit();	
				}	
			}

			$STH = DB::PDO()->prepare('
				INSERT INTO `' . DB_PREFIX . '_comments` SET
				`user_id`      	= ?,
				`type`      	= ?,
				`refid`         = ?,
				`text`        	= ?,
				`time`      	= ?,
				`refidComment` = ?'
			);

			$STH->execute(array(
				Vars::$USER['id'],
				self::$setting['type'],
				self::$setting['refid'],
				$text,
				time(),
				$_POST['refidComment']
			));
			
			$STH = NULL;
			header('location: http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
			exit();
		}
	}
	
	//	Количество комментариев

	private static function countComments(){
		return DB::PDO()->query("SELECT COUNT(*) FROM `".DB_PREFIX."_comments` WHERE
			`type`='" . self::$setting['type'] . "' AND
			`refid`= '" . self::$setting['refid'] . "'")->fetchColumn();

	}

	// Получаем комментарии

	private static function getComments(){
		return DB::PDO()->query("SELECT *, `" . DB_PREFIX . "_comments`.`id` AS `idcomment` 
			FROM `".DB_PREFIX."_comments`
			LEFT JOIN `" . DB_PREFIX ."_users_profile` ON `" . DB_PREFIX ."_comments`.`user_id` = `" . DB_PREFIX ."_users_profile`.`id`
			WHERE `type`='" . self::$setting['type'] . "' AND `refid`= '" . self::$setting['refid'] . "'
			ORDER BY time DESC
		");
	}
	//	Проверка на существование сообщения
	
	// private function isComment($id = 0) {
		// return DB::PDO()->query("SELECT COUNT(*) FROM `".DB_PREFIX."_comments` WHERE
			// `id`='" . $id . "' AND
			// `type`='" . self::$setting['type'] . "' AND
			// `refid`= '" . self::$setting['refid'] . "'")->fetchColumn();		
	// }	
	
}