<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');

class Validate
{
    public $error = array();
    public $is = false;

    public function __construct($type, $value, array $option = array()) {
        if (method_exists($this, $type)) {
            $option['value'] = $value;
            $this->is = call_user_func(array($this, $type), $option);
        } else {
            $this->error[] = 'Unknown Validator';
        }
    }

    // Валидация длины строки

    private function lenght(array $option) {
        if (isset($option['empty']) && $option['empty'] && empty($option['value'])) {
            return true;
        }

		if (isset($option['min']) && mb_strlen($option['value']) < $option['min']) {
            $this->error[] = 'Минимум ' . $option['min'] . ' символов';
            return false;
		} elseif (isset($option['max']) && mb_strlen($option['value']) > $option['max']) {
            $this->error[] = 'Максимум ' . $option['max'] .  ' символов';
            return false;
		}

        return true;
    }

    // Валидация числового значения

	private function numeric(array $option) {
        if (isset($option['empty']) && $option['empty'] && empty($option['value'])) {
            return true;
        }

        if (!is_numeric($option['value'])) {
            $this->error[] = 'Неверные данные';

            return false;
        }

        if (isset($option['min']) && $option['value'] < $option['min']) {
            $this->error[] = 'Минимум ' . $option['min'];

            return false;
        } elseif (isset($option['max']) && $option['value'] > $option['max']) {
            $this->error[] = 'Максимум ' . $option['max'];

            return false;
        }

        return true;
    }

    // Проверка символов

	private function checkCharacter(array $option) {
        if (isset($option['empty']) && $option['empty'] && empty($option['value'])) {
            return true;
        }

		if (preg_match('/[^' . $option['character'] . ']+/', $option['value'])){
            $this->error[] = 'Недопустимые символы';

            return false;
        }

		return true;
    }

    // Валидация длины строки

    private function checkUniqueValue(array $option) {
        if ((isset($option['empty']) && $option['empty'] && empty($option['value'])) || $option['value_old'] == $option['value']) {
            return true;
        }

		$total = DB::PDO()->query("SELECT COUNT(*) FROM `" . $option['table_db'] . "` WHERE `" . $option['field'] . "`='" . $option['value'] . "'")->fetchColumn();

		if($total) {           $this->error[] = 'Такой ключ уже есть базе данных';

            return false;
		}

		return true;
    }

	//Проверка кода с картинки

	function captcha($var, $var2) {
		$error = false;

		if(!$var) {
			$error = 'Введите код с картинки';
		} elseif($var != $var2){
			$error = 'Неверный код с картинки';
		}

		return $error;
	}

	//Проверка ника на соответствующие правила

	function login($var) {
		$error = false;

		if(empty($var)){
			$error = 'Логин пустой';
		} elseif(mb_strlen($var) < 4 || mb_strlen($var) > 20){
            $error = 'Логин недопустимой длины';
        } elseif (preg_match('/[^\da-zA-Z\-\@\*\(\)\?\!\~\_\=\[\]]+/', $var)){
            $error = 'Недопустимые символы в Логине';
        } elseif(filter_var($var, FILTER_VALIDATE_INT) !== false){
            $error = 'Логин не может состоять только из цифр';
        } elseif(preg_match("/(.)\\1\\1\\1/", $var)){
            $error = 'Логины с повторяющимися символами недоступны';
        } else {
        	$data = DB::PDO()->query("SELECT * FROM `" . DB_PREFIX . "_users_profile` WHERE `login` = '{$var}' LIMIT 1")->fetch();
			if(!empty($data)){
				$error = 'Логин занят, выберите другой';
        	}
        }

		return $error;
	}

	//Проверка пароля на соответствие

	function password($var) {
		$error = false;

        if(empty($var)){
            $error = 'Пустой пароль';
        } elseif(mb_strlen($var) < 6 || mb_strlen($var) > 20){
            $error = 'Недопустимая длина';
        } elseif(preg_match('/[^\da-z]+/i', $var)){
            $error = 'Недопустимый диапозон символов в пароле';
        }

        return $error;
	}

	//Проверка повторного пароля

	function passwordRepeat($var, $var2) {
		$error = false;

		if(empty($var2)){
			$error = 'Введите подтверждение пароля';
		} elseif($var != $var2){
			$error = 'Пароли не совпадают';
		}

		return $error;
	}

	//Проверка email

	function email($var) {
        $error = false;

		if(empty($var)){
			$error = 'Пустой email';
		} elseif(!filter_var($var, FILTER_VALIDATE_EMAIL)){
			$error = 'Неккоректный email адрес';
		}

		return $error;
	}

	//Проверка соглашения по правилам

	function regulations($var) {
		if(!$var){
			$error = 'Вы должны согласиться с правилами, что бы зарегистрироваться на сайте';
		} else {
			$error = false;
		}

		return $error;
	}

}
