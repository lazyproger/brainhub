<?php

class Form
{
    private $_form;
    private $_fields = array();
    private $_submits = array();
    private $_fieldset = FALSE;
    private $_rules = array();
    private $_validate = TRUE;

    public $input;
    // public $validationToken = TRUE;
    public $isSubmitted = FALSE;
    public $isValid = FALSE;
    public $output = array();

    public $infoMessages = '<div class="alert %s">%s</div>';
    public $confirmation = FALSE;
    public $continueLink;
    public $successMessage;
    public $errorMessage;

    public function getInstance($action, $method = '', $name = 'form')
    {
        $this->_form = array( 
            'type'   => 'form',
            'name'   => $name,
            'action' => $action,
            'method' => ($method == 'get' ? 'get' : 'post')
        );

        $this->successMessage = 'Выполнено';
        $this->errorMessage = 'Ошибка';

        // Передаем по ссылке значения суперглобальных переменных
        if ($this->_form['method'] == 'get') {
            $this->input =& $_GET;
        } else {
            $this->input =& $_POST;
        }
    }

    public function __toString()
    {
        return $this->display();
    }

    /**
     * Добавляем элементы формы
     *
     * @param string $type              Тип добавляемого элемента
     * @param string $name              Имя элемента
     * @param array  $option            Дополнительные параметры
     *
     * @return Form
     */
    public function element($type, $name, array $option = array())
    {
        if ($type == 'submit') {
            $this->_submits[] = $name;
        } elseif ($type == 'file') {
            $this->_form['enctype'] = TRUE;
        } elseif ($type == 'textarea' && !isset($option['rows'])) {
            $option['rows'] = Vars::$USER_SET['field_h'];
        }

        $option['type'] = $type;
        $option['name'] = $name;
        $this->_fields[$name] = $option;

        unset($option);

        return $this;
    }

    /**
     * Добавляем HTML код
     * Строка не обрабатывается и передается в форму как есть.
     *
     * @param $str
     *
     * @return Form
     */
    public function html($str)
    {
        $this->_fields[] = array(
            'type'    => 'html',
            'content' => $str
        );

        return $this;
    }

    /**
     * Добавление блока fieldset
     *
     * @param string $legend
     *
     * @return Form
     */
    public function fieldset($legend = NULL)
    {
        if ($this->_fieldset) {
            $this->_fields[] = array(
                'type'    => 'html',
                'content' => '</fieldset>'
            );
        }

        $this->_fields[] = array(
            'type'    => 'html',
            'content' => '<fieldset>'
        );

        if (!is_null($legend)) {
            $this->_fields[] = array(
                'type'    => 'html',
                'content' => '<legend>' . $legend . '</legend>'
            );
        }

        $this->_fieldset = TRUE;

        return $this;
    }

    /**
     * Принудительное закрытие блока fieldset
     *
     * @return Form
     */
    public function fieldsetEnd()
    {
        $this->_fields[] = array(
            'type'    => 'html',
            'content' => '</fieldset>'
        );

        $this->_fieldset = FALSE;

        return $this;
    }

    /**
     * Добавление блока CAPTCHA
     *
     * @return $this
     */
    public function captcha()
    {
        $this->_fields[] = array(
            'type'    => 'html',
            'content' => '<img src="' . Vars::$HOME_URL . 'assets/captcha/captcha.php" alt="' . __('captcha_help') . '"/><br/>'
        );

        return $this;
    }

    /**
     * Добавление правил валидации
     *
     * @param string $field
     * @param string $type
     * @param array  $options
     *
     * @return Form
     */
    public function validate($field, $type, $options = array())
    {
        $options['field'] = $field;
        $options['type'] = $type;
        $this->_rules[] = $options;

        return $this;
    }

    /**
     * Обработка данных формы
     *
     * @return bool
     */
    public function process()
    {
        // Проверка формы на Submit
        if (count(array_intersect($this->_submits, array_keys($this->input)))
            // && (!$this->validationToken || isset($this->input['form_token']))
            // && (!$this->validationToken || isset($_SESSION['form_token']))
            // && (!$this->validationToken || $this->input['form_token'] == $_SESSION['form_token'])
        ) {
            $this->isSubmitted = TRUE;
            $this->isValid = TRUE;
        }

        if ($this->isSubmitted) {
            // Присваиваем значения VALUE
            foreach ($this->_fields as &$element) {
                $this->_setValues($element);
            }

            // Валидация данных
            foreach ($this->_rules as $validator) {
                if ($this->_validate && array_key_exists($validator['field'], $this->_fields)) {
                    $this->_validateField($validator, $this->_fields[$validator['field']]);
                }
            }

            if ($this->isValid) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Заключительная сборка готовой формы
     *
     * @return string
     */
    public function display()
    {
        // Информационные сообщения об ошибке, или успехе
        $message = '';
        if ($this->infoMessages !== FALSE && $this->isSubmitted || isset($_GET['saved'])) {
            if ($this->isValid || isset($_GET['saved'])) {
                // Сообщение об удачном сохранении данных
                $message = sprintf($this->infoMessages, 'alert-success', $this->successMessage);
                if ($this->confirmation) {
                    // Если задано отдельное окно подтверждения
                    $message .= '<a class="btn btn-primary btn-large" href="' . (is_null($this->continueLink) ? Vars::$HOME_URL : $this->continueLink) . '">Продолжить</a>';

                    return $message;
                }
            } else {
                // Сообщение, что имеются ошибки
                $message = sprintf($this->infoMessages, 'alert-danger', $this->errorMessage);
            }
        }

        $out = array();
        foreach ($this->_fields as &$element) {
            // Создаем элемент формы
            if ($element['type'] == 'html') {
                $out[] = $element['content'];
            } else {
                if($this->isSubmitted && isset($element['reset_value'])){
                    $element['value'] = $element['reset_value'];
                }
                $out[] = new Fields($element['type'], $element['name'], $element, $this->_form['name']);
            }
        }

        if ($this->_fieldset) {
            $out[] = '</fieldset>';
            $this->_fieldset = FALSE;
        }

        unset($this->_fields);

        // Добавляем токен валидации
        /*
        if ($this->validationToken) {
            if (!isset($_SESSION['form_token'])) {
                $_SESSION['form_token'] = Functions::generateToken();
            }
            $out[] = new Fields('hidden', 'form_token', array('value' => $_SESSION['form_token']));
        }
        */

        return sprintf("\n" . $message . "\n" . '<form action="%s" method="%s" name="%s"%s>%s</form>' . "\n",
            $this->_form['action'],
            $this->_form['method'],
            $this->_form['name'],
            (isset($this->_form['enctype']) ? ' enctype="multipart/form-data"' : ''),
            "\n" . implode("\n", $out) . "\n"
        );
    }

    /**
     * Присвоение полям формы значений value после Submit
     *
     * @param array $option
     */
    private function _setValues(array &$option)
    {
        switch ($option['type']) {
            case'html':
                break;

            case'text':
            case'password':
            case'hidden':
            case'textarea':
                if (isset($this->input[$option['name']])) {
                    $option['value'] = trim($this->input[$option['name']]);
                    unset($this->input[$option['name']]);

                    // Применяем фильтры
                    if (isset($option['filter'])) {
                        $this->_filter($option);
                    }

                    if (isset($option['required']) && empty($option['value'])) {
                        // Проверка на обязательное поле
                        $option['error'] = 'Не заполненое поле';
                        $this->_validate = FALSE;
                        $this->isValid = FALSE;
                    }

                    $this->output[$option['name']] = $option['value'];
                } else {
                    $this->isValid = FALSE;
                }
                break;

            case'radio':
                if (isset($this->input[$option['name']]) && isset($option['items'])) {
                    if (array_key_exists($this->input[$option['name']], $option['items'])) {
                        $option['checked'] = trim($this->input[$option['name']]);
                        $this->output[$option['name']] = $option['checked'];
                        unset($this->input[$option['name']]);
                    } else {
                        $this->isValid = FALSE;
                    }
                }
                break;

            case'select':
                if (isset($this->input[$option['name']]) && isset($option['items'])) {
                    $allow = TRUE;
                    if (isset($option['multiple']) && $option['multiple']) {
                        foreach ($this->input[$option['name']] as $val) {
                            if (!array_key_exists($val, $option['items'])) {
                                $allow = FALSE;
                                break;
                            }
                        }
                    } else {
                        if (!array_key_exists($this->input[$option['name']], $option['items'])) {
                            $allow = FALSE;
                        }
                    }

                    if ($allow) {
                        $option['selected'] = $this->input[$option['name']];
                        $this->output[$option['name']] = $option['selected'];
                        unset($this->input[$option['name']]);
                    } else {
                        $this->isValid = FALSE;
                    }
                }
                break;

            case'checkbox':
                if (isset($this->input[$option['name']])) {
                    unset($this->input[$option['name']]);
                    $option['checked'] = 1;
                    $this->output[$option['name']] = 1;
                } else {
                    unset($option['checked']);
                    $this->output[$option['name']] = 0;
                }
                break;
        }
    }

    /**
     * Фильтрация значений полей формы после Submit
     *
     * @param $option
     */
    private function _filter(&$option)
    {
        $min = isset($option['filter']['min']) ? intval($option['filter']['min']) : FALSE;
        $max = isset($option['filter']['max']) ? intval($option['filter']['max']) : FALSE;

        switch ($option['filter']['type']) {
            case'str':
            case'string':
                if (isset($option['filter']['regexp_search'])) {
                    $replace = isset($option['filter']['regexp_replace']) ? $option['filter']['regexp_replace'] : '';
                    $option['value'] = preg_replace($option['filter']['regexp_search'], $replace, $option['value']);
                }
                if ($max) {
                    $option['value'] = mb_substr($option['value'], 0, $max);
                }
                break;

            case'int':
            case'integer':
                $option['value'] = intval($option['value']);
                if ($min !== FALSE && $option['value'] < $min) {
                    $option['value'] = $min;
                }
                if ($max !== FALSE && $option['value'] > $max) {
                    $option['value'] = $max;
                }
                break;

            default:
                $option['error'] = 'Unknown filter: ' . $option['filter']['type'];
        }
    }

    /**
     * Валидация полей формы
     *
     * @param array $validator
     * @param array $option
     *
     * @uses Validate
     */
    private function _validateField(array $validator, array &$option)
    {
        if (isset($validator['valid']) && $validator['valid'] && !$this->isValid) {
            return;
        }

        if ($validator['type'] == 'compare') {
            if (isset($this->output[$validator['compare_field']])) {
                $validator['compare_value'] = $this->output[$validator['compare_field']];
            } else {
                $option['error'] = 'ERROR: compare field does not exist';
            }
        }

        $check = new Validate($validator['type'], $option['value'], $validator);

		if ($check->is !== TRUE) {
			if (isset($option['error']) && !empty($option['error'])) {
                if (is_array($option['error'])) {
                    $option['error'] = array_merge($option['error'], $check->error);
                } else {
                    $option['error'] = $option['error'] . '<br/>' . implode('<br/>', $check->error);
                }
            } else {
                $option['error'] = $check->error;
            }

            $this->isValid = FALSE;

            if (isset($validator['continue']) && !$validator['continue']) {
                $this->_validate = FALSE;
            }
		}

        unset($check);
    }
}