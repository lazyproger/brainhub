<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');


class Text {

	// Главная обработка текста
	public static function main($str, $br = 1, $html = false){
		
        $str = str_replace("\r\n", ' ', $str);
       	

    	if(!$html) {
			$str = self::filterString($str, 2);
    	}
		
		$str = html_entity_decode($str);
		// $str = strip_tags($str, '<br><img><p>');
		
		$str = self::embed($str);
		// $str = self::bbcode($str);
		$str = self::highlightImages($str);
		
		return $str;
	}
	
    //Обрабатываем текст пришедший из TinyMce
    public static function editor($text){
        $text = preg_replace('/style="(.*?)"/', '', $text);
        $text = preg_replace('/name="(.*?)"/', '', $text);

        
        $text = str_replace("
            ", '', $text);
        $text = str_replace(
            array(
                "<p><br>", 
                "  ", 
                "<br />", 
                "<p><br>", 
                "<br><br>", 
                "<br></p>",
                " >",
                "&ndash;",
                "&mdash;",
                "h2>",
                "h1>",
                "h4>",
                "h5>",
                "h6>",
                "&nbsp;",
                "../../"
            ),
            array(
                "<p>", 
                " ", 
                "<br>", 
                "<p>", 
                "<br>",
                "</p>",
                ">",
                "-",
                "-",
                "h3>",
                "h3>",
                "h3>",
                "h3>",
                "h3>",
                "",
                "/"
            ), $text);

        return $text;
    }

    //TODO: переместить функцию отсюда
    public static function uploadImage($file, $dir, $name, $settings = ''){
        $handle = new upload($file);
        if(!isset($settings['width'])){
            $settings['width'] = 1980;
        }
        $name = str_replace(array('.png', '.gif', '.jpg', '.jpeg'), '', $name);
        $typeImage = $file;
        if ($handle->uploaded) {
            // Обрабатываем фото
            $handle->file_new_name_body = $name;
            $handle->allowed = array('image/jpeg', 'image/gif', 'image/png');
            $handle->file_max_size = 1024 * 2048000 * 10;
            $handle->image_resize = true;
            $handle->image_ratio_y = true;
            $handle->image_no_enlarging = true;
            $handle->image_x = $settings['width'];
            $handle->process($dir);
            if ($handle->processed) {
                $handle->file_new_name_body = $name.'_550';
                $handle->allowed = array('image/jpeg', 'image/gif', 'image/png');
                $handle->image_resize = true;
                $handle->image_ratio_y = true;
                $handle->image_no_enlarging = true;
                $handle->image_x = 550;
                $handle->process($dir);
                if ($handle->processed) {
                    $handle->file_new_name_body = $name.'_320';
                    $handle->allowed = array('image/jpeg', 'image/gif', 'image/png');
                    $handle->image_resize = true;
                    $handle->image_ratio_y = true;
                    $handle->image_no_enlarging = true;
                    $handle->image_x = 320;
                    $handle->process($dir);
                    if ($handle->processed) {
                        return true;
                    }
                }else{
                    return $handle->error;
                }
            }else{
                return $handle->error;
            }
            $handle->clean();
        }else{
            return $handle->error;
        }
    }

    //Получаем все картинки из текста
    public static function returnCollection($content){
        preg_match_all('/<img[^>]+>/i',$content, $result); 
        
        if(count($result[0]) > 0){
        
            $return = array();
            foreach( $result[0] as $img_tag)
            {
                preg_match_all('/(src)=("[^"]*")/i',$img_tag, $img[]);

            }
            $count = count($img);
            for($i=0;$i<$count;$i++){
                $img[$i][2][0] = str_replace('"', '', $img[$i][2][0]);
                
                $return[] = array(
                    'src' => $img[$i][2][0],
                );
            }
            
            return $return;
        }else{
            return false;
        }
        
        
    }

	public static function amp($str){
		$str = str_replace("\r\n", ' ', $str);
		
		$str = self::highlightImagesAmp($str);
		return $str;
	}

	public static function embed($text){
	
		$pattern = "/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/";

		return preg_replace_callback($pattern, 'self::embed_activate', $text);
	}

	public static function embed_activate($match){
		global $redis;
		
		$domain = parse_url($match[0], PHP_URL_HOST);

		$whiteList = array(
			'twitter.com',
			'www.youtube.com',
			'www.instagram.com'
		);
		
		if(in_array($domain, $whiteList)){
		
			$redislogic = Cache::get('embed_'.$match[0].'') ? Cache::get('embed_'.$match[0].'') : false;
				
			if($redislogic)
				return ($redislogic);	
			else
				return '<div class="embed">'.$match[0].'</div>';
		
		}else{
			return $match[0];
		}
	}
		

	// Обработка строки
	public static function filterString($str, $type = false) {

		if(!$type || $type == 1) {
			$str = htmlentities(trim($str), ENT_QUOTES, 'UTF-8');
	    }

		if(!$type || $type == 2) {
			$str = strtr($str, array(chr(0) => '', chr(1) => '', chr(2) => '', chr(3) => '', chr(4) => '', chr(5) => '', chr(6) => '',
	        chr(7) => '', chr(8) => '', chr(9) => '', chr(10) => '', chr(11) => '', chr(12) => '', chr(13) => '', chr(14) => '', chr(15) => '',
	        chr(16) => '', chr(17) => '', chr(18) => '', chr(19) => '', chr(20) => '', chr(21) => '', chr(22) => '', chr(23) => '', chr(24) => '',
	        chr(25) => '', chr(26) => '', chr(27) => '', chr(28) => '', chr(29) => '', chr(30) => '', chr(31) => ''));
	   	}

	    return $str;
	}

	// Парсинг картинок
	public static function highlightImages($var) {

		$var = preg_replace_callback('#\[img src="(.*?)"]#s', function($file){
			
			$i320 = str_replace('.jpg', '_320.jpg', $file[1]);
			$i550 = str_replace('.jpg', '_550.jpg', $file[1]);
			
			return '<picture>
				<source srcset="'.$i320.'" media="(max-width: 375px)">
				<source srcset="'.$i550.'" media="(max-width: 768px)">
				<source srcset="'.$file[1].'">
				<img srcset="'.$file[1].'" alt=""/>
			</picture>';
			
		}, $var);

		return $var;
	}
	
	// Парсинг картинок
	public static function highlightImagesAmp($var) {

		$var = preg_replace_callback('#\[img src="(.*?)"]#s', function($file){
			
	
			$i550 = str_replace('.jpg.jpg', '.jpg_550.jpg', $file[1]);
				
			return '<amp-img src="'.$i550.'" alt="" width="550" height="300"></amp-img>';
				
		}, $var);

		return $var;
	}

	// Парсинг ссылок
   	public static function highlightUrl($text)
    {
        if (!function_exists('url_callback')) {
            function url_callback($type, $whitespace, $url, $relative_url)
            {
                $orig_url = $url;
                $orig_relative = $relative_url;
                $url = htmlspecialchars_decode($url);
                $relative_url = htmlspecialchars_decode($relative_url);
                $text = '';
                $chars = array('<', '>', '"');
                $split = false;
                foreach ($chars as $char) {
                    $next_split = strpos($url, $char);
                    if ($next_split !== false) {
                        $split = ($split !== false) ? min($split, $next_split) : $next_split;
                    }
                }
                if ($split !== false) {
                    $url = substr($url, 0, $split);
                    $relative_url = '';
                } else if ($relative_url) {
                    $split = false;
                    foreach ($chars as $char) {
                        $next_split = strpos($relative_url, $char);
                        if ($next_split !== false) {
                            $split = ($split !== false) ? min($split, $next_split) : $next_split;
                        }
                    }
                    if ($split !== false) {
                        $relative_url = substr($relative_url, 0, $split);
                    }
                }
                $last_char = ($relative_url) ? $relative_url[strlen($relative_url) - 1] : $url[strlen($url) - 1];
                switch ($last_char)
                {
                    case '.':
                    case '?':
                    case '!':
                    case ':':
                    case ',':
                        $append = $last_char;
                        if ($relative_url) $relative_url = substr($relative_url, 0, -1);
                        else $url = substr($url, 0, -1);
                        break;

                    default:
                        $append = '';
                        break;
                }
                $short_url = (strlen($url) > 40) ? mb_substr($url, 0, 30) . ' ... ' . substr($url, -5) : $url;

                switch ($type) {
                    case 1:
                        $relative_url = preg_replace('/[&?]sid=[0-9a-f]{32}$/', '', preg_replace('/([&?])sid=[0-9a-f]{32}&/', '$1', $relative_url));
                        $url = $url . '/' . $relative_url;
                        $text = $relative_url;
                        if (!$relative_url) {
                            return $whitespace . $orig_url . '/' . $orig_relative;
                        }
                        break;
                    case 2:
                        $text = $short_url;
                        //$url = '/pages/redirect.html?url=' . rawurlencode($url);
                        break;
                     case 3:
                        $url = 'http://' . $url;
                        $text = $short_url;
                        //$url = $home_page . '/pages/redirect.html?url=' . rawurlencode($url);
                        break;
                    case 4:
                        $text = $short_url;
                        $url = 'mailto:' . $url;
                        break;
                }

                $url = htmlspecialchars($url);
                $text = htmlspecialchars($text);
                $append = htmlspecialchars($append);
                return $whitespace . '<a href="' . $url . '">' . $text . '</a>' . $append;
            }
        }

        static $url_match;
        static $url_replace;

        if (!is_array($url_match)) {
            $url_match = $url_replace = array();

            // Обработка внутренние ссылки
            $url_match[] = '#(^|[\n\t (>.])(' . preg_quote(URL, '#') . ')/((?:[a-zа-яё0-9\-._~!$&\'(*+,;=:@|]+|%[\dA-F]{2})*(?:/(?:[a-zа-яё0-9\-._~!$&\'(*+,;=:@|]+|%[\dA-F]{2})*)*(?:\?(?:[a-zа-яё0-9\-._~!$&\'(*+,;=:@/?|]+|%[\dA-F]{2})*)?(?:\#(?:[a-zа-яё0-9\-._~!$&\'(*+,;=:@/?|]+|%[\dA-F]{2})*)?)#iu';
            $url_replace[] = "url_callback(1, '\$1', '\$2', '\$3')";

            // Обработка обычных ссылок типа xxxx://aaaaa.bbb.cccc. ...
            $url_match[] = '#(^|[\n\t (>.])([a-z][a-z\d+]*:/{2}(?:(?:[a-zа-яё0-9\-._~!$&\'(*+,;=:@|]+|%[\dA-F]{2})+|[0-9.]+|\[[a-zа-яё0-9.]+:[a-zа-яё0-9.]+:[a-zа-яё0-9.:]+\])(?::\d*)?(?:/(?:[a-zа-яё0-9\-._~!$&\'(*+,;=:@|]+|%[\dA-F]{2})*)*(?:\?(?:[a-zа-яё0-9\-._~!$&\'(*+,;=:@/?|]+|%[\dA-F]{2})*)?(?:\#(?:[a-zа-яё0-9\-._~!$&\'(*+,;=:@/?|]+|%[\dA-F]{2})*)?)#iu';
            $url_replace[] = "url_callback(2, '\$1', '\$2', '')";

            // Обработка сокращенных ссылок, без указания протокола "www.xxxx.yyyy[/zzzz]"
            $url_match[] = '#(^|[\n\t (>])(www\.(?:[a-zа-яё0-9\-._~!$&\'(*+,;=:@|]+|%[\dA-F]{2})+(?::\d*)?(?:/(?:[a-zа-яё0-9\-._~!$&\'(*+,;=:@|]+|%[\dA-F]{2})*)*(?:\?(?:[a-zа-яё0-9\-._~!$&\'(*+,;=:@/?|]+|%[\dA-F]{2})*)?(?:\#(?:[a-zа-яё0-9\-._~!$&\'(*+,;=:@/?|]+|%[\dA-F]{2})*)?)#iu';
            $url_replace[] = "url_callback(3, '\$1', '\$2', '')";
        }
        return preg_replace($url_match, $url_replace, $text);
    }


	//Обрезка текста
	public static function pruning($text, $max, $end = '...'){
		$strlen = strlen($text);

		while(substr($text, $max, 1) != ' ' and $max < $strlen) {
			$max++;
		}

		$text = substr($text, 0, $max);

		if($max < $strlen) {
			$text .= $end;
		}

		return $text;
	}
	
	public static function replaceImgInTag($content){
		
		
		preg_match_all('/<img[^>]+src="?\'?([^"\']+)"?\'?[^>]*>/i', $content, $result);
		
		// print_r($result);
		$countImage = count($result[0]);

		for($i=0;$i<$countImage;$i++){
			
			
			$content = str_replace($result[0][$i], '[img src="'.$result[1][$i].'"]', $content);
		}
		
		
		
		
		return $content;
	
	}
	

}
