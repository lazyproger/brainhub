<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');


class Cache {


	//Получение по ключу
	public static function get($key){
		$data = DB::PDO()->query("SELECT `content` FROM `cache` WHERE `key` = '{$key}'")->fetchColumn();
		if($data){
			return $data;
		}else{
			return false;
		}
	}

	//Установка кеша
	public static function set($key, $content){
		$cache = DB::PDO()->query("SELECT `content` FROM `cache` WHERE `key` = '{$key}'")->fetchColumn();

		//Если кеш найден, обновляем
		if($cache){
			$STH = DB::PDO()->prepare("
			UPDATE `cache`
			SET `content` = ?,
			`time` = ?
			WHERE `key` = ?"
			);

			$STH->execute(array(
				$content, Vars::$TIME, $key
			));

		//Иначе добавляем
		}else{
			$STH = DB::PDO()->prepare('
			INSERT INTO `cache` SET
				`key`     = ?,
				`content` = ?,
				`time`	  = ?'
			);

			$a = $STH->execute(array(
				$key, $content, Vars::$TIME
			));

			return true;
		}

	}

}
