<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');

class Functions
{
	
	// Получаем категорию
	public static function getCategory($id){
		$query = DB::PDO()->query("SELECT * FROM `brhb_categories` WHERE 
		`id_category` = '".$id."' LIMIT 1")->fetch();
		
		return $query;
	}
	// Получаем категорию
	public static function getArticle($id){
		$query = DB::PDO()->query("SELECT * FROM `article` WHERE 
		`id_article` = '".$id."' LIMIT 1")->fetch();
		
		return $query;
	}
	
	
    // Показываем дату с учетом сдвига времени
	public static function displayDate($time) {
		$time = $time + 3600 * 3;
		
		if($time > time()){
			return date('d.m.Y H:i', $time);
		}
		
        $month = date( 'm',$time );
		$day   = date( 'd',$time );
        $year  = date( 'y',$time );
        $hour  = date( 'G',$time );
        $min   = date( 'i',$time );

                $date = $day . '.' . $month . '.' . $year . '';

                $dif = (time() + (3600 * 3))- $time;
				// return date('H:i:s', time() + (3600 * 3)).'--'.date('H:i:s', $time);
                if($dif<59){
                    return $dif." сек. назад";
                }elseif($dif/60>1 and $dif/60<59){
                    return round($dif/60)." мин. назад";
                }elseif($dif/3600>1 and $dif/3600<23){
                    return round($dif/3600)." час. назад";
                }else{
                    return $date;
    }
	}
	
	//Ссылки на авторизации через соц.сети
	public static function socialAuthLink($type){
		$array = constant($type);

		$params = array(
			'client_id'     => $array['ID'],
			'redirect_uri'  => $array['REDIRECT'],
			'response_type' => 'code'
		);
		
		return $array['URL'].'?'.urldecode(http_build_query($params));
	
	}
	
	
    // Склоняем русские слова
    public static function bowWords($value = 0, $words = 0) {
        if ($value < 11 or $value > 19) {
            $value = (string )$value;
            switch ($value{mb_strlen($value) - 1}) {
                case 1:
                    return ($words[0]);
                    break;
                case 2:
                case 3:
                case 4:
                    return ($words[1]);
                    break;
                default:
                    return ($words[2]);
                    break;
            }
        } else
            return ($words[2]);
    }

    // Формируем alias
    public static function getAlias($str) {
    	$str = mb_strtolower($str, 'UTF-8');
        $str = preg_replace ("/[^a-zа-я\-\d\s]/u"," ", $str);
        $str = trim(preg_replace("/( {2,})/", " ", $str));

		$str = strtr($str, array(
				' ' => '_', 'ё' => 'yo', 'й' => 'ii',
				'ц' => 'c', 'у' => 'u', 'к' => 'k', 'е' => 'e', 'н' => 'n', 'г' => 'g',
				'ш' => 'w', 'щ' => 'sh', 'з' => 'z', 'х' => 'x', 'ъ' => '', 'ф' => 'f',
				'ы' => 'y', 'в' => 'v', 'а' => 'a', 'п' => 'p', 'р' => 'r', 'о' => 'o',
				'л' => 'l', 'д' => 'd', 'ж' => 'zh', 'э' => 'ee', 'я' => 'ya', 'ч' => 'ch',
				'с' => 's', 'м' => 'm', 'и' => 'i', 'т' => 't', 'ь' => '', 'б' => 'b', 'ю' => 'yu'
		));

    	return $str;
	}
	
    // Формируем alias
    public static function getAlias2($str) {
    	$str = mb_strtolower($str, 'UTF-8');
        // $str = preg_replace ("/[^a-zа-я\-\d\s]/u"," ", $str);
        $str = trim(preg_replace("/( {2,})/", " ", $str));

		$str = strtr($str, array(
				' ' => '_', 'ё' => 'yo', 'й' => 'ii',
				'ц' => 'c', 'у' => 'u', 'к' => 'k', 'е' => 'e', 'н' => 'n', 'г' => 'g',
				'ш' => 'w', 'щ' => 'sh', 'з' => 'z', 'х' => 'x', 'ъ' => '', 'ф' => 'f',
				'ы' => 'y', 'в' => 'v', 'а' => 'a', 'п' => 'p', 'р' => 'r', 'о' => 'o',
				'л' => 'l', 'д' => 'd', 'ж' => 'zh', 'э' => 'ee', 'я' => 'ya', 'ч' => 'ch',
				'с' => 's', 'м' => 'm', 'и' => 'i', 'т' => 't', 'ь' => '', 'б' => 'b', 'ю' => 'yu', '(' => '-', ')' => '-', '[' => '', ']' => ''
		));

    	return $str;
	}

	// Навигация по страницам
	public static function displayPagination($url, $start, $total, $pagesize) {
        $neighbors = 1;
        if ($start >= $total) {
            $start = max(0, (int)$total - (((int)$total % (int)$pagesize) == 0 ? $pagesize : ((int)$total % (int)$pagesize)));
        } else {
            $start = max(0, (int)$start - ((int)$start % (int)$pagesize));
        }
        $base_link = '<a class="btn%s" href="' . strtr($url, array('%' => '%%')) . 'page=%d' . '">%s</a>';

        // Кнопка "Назад"
        $out[] = $start == 0 ? '' : '<a class="btn previous" href="' . $url . 'page=' . ($start / $pagesize) . '">«</a>';

        // Кнопка 1-й страницы
        if ($start > $pagesize * $neighbors) {
            $out[] = '<a class="btn pbtn" href="' . $url . 'page=1">1</a>';
        }

        if ($start > $pagesize * ($neighbors + 1)) {
            $out[] = ' ';
        }

        // Кнопки слева от текущей
        for ($nCont = $neighbors; $nCont >= 1; $nCont--) {
            if ($start >= $pagesize * $nCont) {
                $tmpStart = $start - $pagesize * $nCont;
                $out[] = '<a class="btn pbtn" href="' . $url . 'page=' . ($tmpStart / $pagesize + 1) . '">' . ($tmpStart / $pagesize + 1) . '</a>';
            }
        }

        // Кнопка текущей страницы
        $out[] = '<a class="btn btn-primary pbtn" href="' . $url . 'page=' . ($start / $pagesize + 1) . '">' . ($start / $pagesize + 1) . '</a>';

        $tmpMaxPages = (int)(($total - 1) / $pagesize) * $pagesize;

        // Кнопки справа от текущей
        for ($nCont = 1; $nCont <= $neighbors; $nCont++) {
            if ($start + $pagesize * $nCont <= $tmpMaxPages) {
                $tmpStart = $start + $pagesize * $nCont;
                $out[] = '<a class="btn pbtn" href="' . $url . 'page=' . ($tmpStart / $pagesize + 1) . '">' . ($tmpStart / $pagesize + 1) . '</a>';
            }
        }

        if ($start + $pagesize * ($neighbors + 1) < $tmpMaxPages) {
            $out[] = ' ';
        }

        // Кнопка последней страницы
        if ($start + $pagesize * $neighbors < $tmpMaxPages) {
            $out[] = '<a class="btn pbtn" href="' . $url . 'page=' . ($tmpMaxPages / $pagesize + 1) . '">' . ($tmpMaxPages / $pagesize + 1) . '</a>';
        }

        // Кнопка "Вперед"
        if ($start + $pagesize < $total) {
            $display_page = ($start + $pagesize) > $total ? $total : ($start / $pagesize + 2);
            $out[] = sprintf($base_link, ' next', $display_page, '»');
        }

        return implode(' ' , $out);
    }

    // Вывод капчи

	public static function captcha() {
		$html = '
		<img id="captcha" src="http://'.URL.'/system/other/captcha/?r='.rand(1000, 9999).'" alt="Если Вы не видите рисунок с кодом, включите поддержку графики в настройках браузера и обновите страницу" title="Нажмите для обновления проверочного изображения"/>';
		return $html;
	}


	public static function time_stamp($session_time) {
		$time_difference = time() - $session_time;
		$hours = round($time_difference / 3600 );
		$days = round($time_difference / 86400 );
		$mounf_array = array('янв', 'фев', 'мар', 'апр', 'мая', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек');
		$date = date("H:i", $session_time);
		$mounf = date("n", $session_time) - 1;
		$dateelse = date("j", $session_time).' '.date($mounf_array[$mounf], $session_time);

		// if($hours <=24)
			// $vr = 'Сегодня в '.$date;
		// elseif($days==1)
			// $vr = 'Вчера в '.$date;
		// else
			$vr = $dateelse;

		return $dateelse;
	}

    
    
    // Сортировка массивов
    
    public static function array_msort($array, $cols) {
        $colarr = array();
        foreach ($cols as $col => $order) {
            $colarr[$col] = array();
            foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
        }
        $eval = 'array_multisort(';
        foreach ($cols as $col => $order) {
            $eval .= '$colarr[\''.$col.'\'],'.$order.',';
        }
        $eval = substr($eval,0,-1).');';
        eval($eval);
        $ret = array();
        foreach ($colarr as $col => $arr) {
            foreach ($arr as $k => $v) {
                $k = substr($k,1);
                if (!isset($ret[$k])) $ret[$k] = $array[$k];
                $ret[$k][$col] = $array[$k][$col];
            }
        }
        return $ret;
    
    }    
	
	//Генерация пароля
	public static function generate_password($number) {  
		$arr = array('a','b','c','d','e','f',  
					 'g','h','i','j','k','l',  
					 'm','n','o','p','r','s',  
					 't','u','v','x','y','z',  
					 'A','B','C','D','E','F',  
					 'G','H','I','J','K','L',  
					 'M','N','O','P','R','S',  
					 'T','U','V','X','Y','Z',  
					 '1','2','3','4','5','6',  
					 '7','8','9','0','.',',',  
					 '(',')','[',']','!','?',  
					 '&','^','%','@','*','$',  
					 '<','>','/','|','+','-',  
					 '{','}','`','~');  
		// Генерируем пароль  
		$pass = "";  
		for($i = 0; $i < $number; $i++)  
		{  
		  // Вычисляем случайный индекс массива  
		  $index = rand(0, count($arr) - 1);  
		  $pass .= $arr[$index];  
		}  
		return $pass;  
	}  
}