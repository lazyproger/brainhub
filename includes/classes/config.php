<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');

class config extends vars
{
	
	// Подключаем config
	public static function init($config)
	{
		if(is_file(ROOTPATH . 'includes/configs/' . $config . '.php')) {
			$CONFIG = array();
			
			require_once(ROOTPATH . 'includes/configs/' . $config . '.php');
			
			if(!empty($CONFIG)) {
				Vars::$CONFIG = array_merge(Vars::$CONFIG, $CONFIG);	
			}
		
		} else {
			die('Config файл ' . $config . '.php не найден');	
		}	
	}
	
	// Проверка наличия ключа в config
	public static function isKey($config, $key)	
	{
		if(isset(Vars::$CONFIG[$config]) && is_array(Vars::$CONFIG[$config])) {
			return isset(Vars::$CONFIG[$config][$key]) ? true : false;	
		} else {
			return false;	
		}	
	}



}