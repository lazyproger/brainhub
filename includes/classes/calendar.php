<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');

class Calendar {

	//Получаем последнее добавленое событие
	public static function lastEvent(){
		$data = DB::PDO()->query("SELECT * FROM `calendar` ORDER BY `id_event` DESC LIMIT 1")->fetch();
		if($data){
			return $data;
		}else{
			return false;
		}
	}
	
	//Показываем шаблон события
	public static function viewEvent($data = ''){
		if(empty($data)){
			$data = self::lastEvent();
		}
		Template::loadTpl('calendar', true, $data); 
	}
	
	public static function checkImage($id, $name = 'logo'){
		
		$logo = false;
		$array = array('.jpg', '.png', '.gif');
		
		foreach($array as $image){
			if(file_exists(ROOTPATH.'/files/events/'.$id.'/'.$name.$image)){
				Template::loadTpl('image', true, '/files/events/'.$id.'/logo_320'.$image); 
				$logo = true;
				break;
			}
		}
		
		return '';		
	}

}

