<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');


class router extends vars
{
	// Обрабатываем основную последовательность маршрутизации
    public function __construct() {
    	$this->getUrl();

		static::$MODULE = $this->getParam(); //Получаем модуль
		static::$MODULE = empty(static::$MODULE) ? 'index' : static::$MODULE;
		static::$MODULE_FILE = $this->getParam(1); //Получаем возможный файл модуля
		static::$ROUTER = $this->getParam(2, 6);
		if(empty(static::$ALIAS)) static::$ALIAS = 'index';
		$this->moduleConnect();

    }

	// Обрабатываем входящие данные
	public static function getUrl()
	{
		if(isset($_GET['query'])){
			if (preg_match('/[^\da-zа-яё\/\_\.\-]+/ui', $_GET['query'])) {
				static::$QUERY[0] = '404';
			} else {
				$query = explode('/', $_GET['query']);
				foreach ($query as $val) {
					if (empty($val) && $val != 0) {
						break;
					}
					static::$QUERY[] = trim($val);
				}
				unset($route);
				static::$QUERY = array_diff(static::$QUERY, array(''));
				static::$QUERY_COUNT = count(static::$QUERY);
				array_walk(static::$QUERY, 'static::getAlias');
			}
		}else{
			static::$QUERY[0] = '';
		}
		
		
	}
	
	//Получаем переменную с алиасом
	public static function getAlias($item, $key){
		static::$ALIAS .= $item.(($key + 1) < static::$QUERY_COUNT ? '_' : '');
	}

	// Получаем параметр по порядковому номеру
	public static function getParam($begin = 0, $end = 0)
	{
		if($end > 0) {
            $out = array();
         	for($i=$begin; $i <= $end; $i++) {
         		$out[] = (isset(static::$QUERY[$i]) ? static::$QUERY[$i] : NULL);
			}
		} else {
	        $out = isset(static::$QUERY[$begin]) ? static::$QUERY[$begin] : NULL;
		}

		return $out;
	}

	// Подключаем модуль
	public static function moduleConnect($module = '') {
		
		$module = empty($module) ? static::$MODULE : $module;
		
		// Ищем в модулях
		$data = DB::PDO()->query("SELECT * FROM `system_router` WHERE `module` = '" . $module . "'")->fetch();
	
		if($data){
            static::$PATH = $data['path'];
		}else{
            $data['path'] = static::$PATH = 'pages';
		}

		// Если есть, подключаем вспомогательный файл
		if(is_file(ROOTPATH . 'modules/' . $data['path'] . '/_inc/helper.php')) {
			include(ROOTPATH . 'modules/' . $data['path'] . '/_inc/helper.php');
   		}
		
		$tmpUrl = ROOTPATH.'modules/'.$data['path'];
		
		//Составляем правила
		$routerArray = array(
			isset(Vars::$QUERY[2]) ? $tmpUrl.'/_inc/'.static::$MODULE_FILE.'/'.Vars::$QUERY[2].'.php' : '',
			$tmpUrl.'/_inc/'.static::$MODULE_FILE.'/index.php',
			$tmpUrl.'/_inc/'.$module.'/'.static::$MODULE_FILE.'.php',
			$tmpUrl.'/_inc/'.static::$MODULE_FILE.'.php',
			$tmpUrl.'/_inc/'.$module.'/index.php',
			$tmpUrl.'/'.$module.'.php',
			$tmpUrl.'/index.php'
		);
		
		//Пробегаем по правилам и если находим файл, подключаем его
		foreach($routerArray as $router){
			if(is_file($router)){
				include($router);
				break;
			}
		}

		



	}

	// Подключаем дополнительные файлы в модуле
	public static function includeFile($key = '', $array = array())
	{
		if (array_key_exists($key, $array) && is_file($array[$key])) {
			define('_IN_MODULE_FILE', 1);
			require_once($array[$key]);
			exit;
		}
	}
	
	// Проверяем свободен ли алиас в таблице _router
	public static function checkNew($alias){
		$data = DB::PDO()->prepare("SELECT * FROM `system_router` WHERE `module` = '" . $alias . "'");
		$data->execute();

		if($data->rowCount() != 0){
			return false;
		}else{
			return true;
		}
	}
	
	// Добавляем новое правило в роутер
	public static function add($array = array()){
		$STH = DB::PDO()->prepare('
		INSERT INTO `system_router` SET
			`module`      	= ?,
			`path`      	= ?,
			`name`      	= ?'
		);

		$STH->execute(array(
			$array['module'],
			$array['path'],
			$array['name']
		));
		
		return true;
	}
	
	//Обновляем алиас по алиасу
	public static function update($old, $new){
		$STH = DB::PDO()->prepare("
		UPDATE `system_router`
		SET `module` = ?
		WHERE `module` = ?"
		);
		
		$STH->execute(array(
			$new,
			$old
		));
		
		return true;
	}
	
	//Удаляем алиас по алиасу
	public static function delete($alias){
		$STH = DB::PDO()->prepare("
		DELETE FROM `system_router`
		WHERE `module` = ?"
		);
		
		$STH->execute(array(
			$alias
		));
		
		return true;
	}
	
}