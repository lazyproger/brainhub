<?php
class Http {
	
	public $user_agent;
	public $accept;
	public $accept_language;
	public $accept_encoding;
	public $accept_charset;
	public $http_connection;
	public $keep_alive;
	public $headers_return_public = false;
	
	private $timeout;
	private $headers;
	private $headers_return;
	private $followlocation;
	private $cookies_path;
	private $referer;
	
	private $response;
	private $response_info;
	
 	public function __construct() {
	}
	
	public function init($timeout = 50, $headers_return = true, $followlocation = true) {
		$this->timeout = $timeout;
		$this->headers_return = $headers_return;
		$this->followlocation = $followlocation;
		
		$this->user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.1) Gecko/20090715 Firefox/3.5.1';
		$this->accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
		$this->accept_language = 'ru-ru';
		$this->accept_encoding = '';
		$this->accept_charset = 'iso-8859-1,utf-8;q=0.7,*;q=0.7';
		$this->http_connection = 'keep-alive';
		$this->keep_alive = 115;
	}
	
	public function setReferer($referer) {
		$this->referer = $referer;
	}
	
	public function setCookies($cookies_path) {
		$this->cookies_path = $cookies_path;
	}
	
	public function setHeaders($headers) {
		$this->headers = $headers;
	}	
	
	public function requestGet($url) {
		return $this->_request($url, null, 'GET');
	}
	
	public function requestPost($url, $vars) {
		return $this->_request($url, $vars, 'POST');
	}
	
	public function getHeaders() {
		if ($this->response && ($this->headers_return || $this->headers_return_public)) {
			$headers = substr($this->response, 0, $this->response_info['header_size']);
			$headers = $this->_httpParseHeaders($headers);
			return $headers;
		}
		return false;
	}
	
	public function getBody() {
		if ($this->response && ($this->headers_return || $this->headers_return_public)) {
			return substr($this->response, $this->response_info['header_size']);
		}
		elseif ($this->response) {
			return $this->response;
		}
		return false;
	}
	
	public function getFilesMultiCurl($files = array(), $mimeTypes = array(), $tmpMimeTypes = array()) {
		$headers = $this->_prepareHeaders();
		$mh = curl_multi_init();	
		$curls_res = array();
	
		foreach($files as $key=>$value) { 
			$curls_res[$key] = curl_init();	
			$curl_options = $this->_curlSetopt($value['url'], $headers, 'GET', array());
			curl_setopt_array($curls_res[$key], $curl_options);
			curl_multi_add_handle($mh, $curls_res[$key]);
		}
	
		$running = NULL;
		do {
			curl_multi_exec($mh, $running);
		} while($running > 0);
	
	
		$result = array();
		foreach ($curls_res as $key=>$value) {
			$result[$key] = array(
				'error'				=> 	curl_error($value),
				'info'				=>	curl_getinfo($value),	
				'file'				=> 	$files[$key]['file'],
				'url'				=> 	$files[$key]['url'],
				'error_mime_type'	=> false,
				'tmp'				=> false
			);
			
			if ($result[$key]['info']['http_code'] != 200) {
				$result[$key]['error'] = true;
			}
			
			if (isset($tmpMimeTypes[$result[$key]['info']['content_type']])) {
				$result[$key]['tmp'] = true;
				$files[$key]['file'] = ROOT_PATH . '/public_html/upload/tmp/' . md5($files[$key]['file']);
				$mimeTypes = array_merge($mimeTypes, $tmpMimeTypes);
			} else if (!isset($mimeTypes[$result[$key]['info']['content_type']])) {
				$result[$key]['error'] = true;
				$result[$key]['error_mime_type'] = true;
			} 			
			
			if(!$result[$key]['error']) {
				$result[$key]['file'] = $files[$key]['file'] . '.' . $mimeTypes[$result[$key]['info']['content_type']];
				$data = curl_multi_getcontent($value);	
				file_put_contents($result[$key]['file'], $data);  
			}
			
			curl_multi_remove_handle($mh, $value);		
		}
		
		curl_multi_close($mh);
		
		return $result;	
	}
	
	private function _request($url, $vars, $method) {
		$headers = $this->_prepareHeaders();
		$curl_options = $this->_curlSetopt($url, $headers, $method, $vars);
        $ch = curl_init();
        curl_setopt_array($ch, $curl_options);
        $this->response = curl_exec($ch);
        if ($this->response) {
        	$this->response_info = curl_getinfo($ch);
        	return array('headers'=>$this->getHeaders(), 'body'=>$this->getBody(), 'info'=>$this->response_info);
       	}
       	return false;
	}
	
	private function _prepareHeaders($referer = null) {
		if(!isset($this->headers)) {
			$this->headers = array(
				'User-Agent' => $this->user_agent,
				'Accept' => $this->accept,
				'Accept-Language' => $this->accept_language,
				'Accept-Encoding' => $this->accept_encoding,
				'Accept-Charset' => $this->accept_charset,
				'Connection' => $this->http_connection,
				'Keep-Alive' => $this->keep_alive,
			);			
		}
		
		if ($this->referer) {
			$this->headers['Referer'] = $this->referer;
		}
		
		$headers = array();
        foreach ($this->headers as $name => $value) {
            $headers[] = $name . ': ' . $value;
        }
        
        return $headers;
	}
	private function _curlSetopt($url, $headers, $method, $vars) {
		$options = array(CURLOPT_URL => $url,
						 CURLOPT_HTTPHEADER => $headers,
						 CURLOPT_REFERER => $url,
						 CURLOPT_HEADER => ($this->headers_return || $this->headers_return_public ? true : false),
						 CURLOPT_TIMEOUT => $this->timeout,
						 CURLOPT_FOLLOWLOCATION => $this->followlocation,
						 CURLOPT_RETURNTRANSFER => true,
		   			);
		
		$ssl = preg_match('/^https/si', $url) ? true : false;
		if ($ssl) {
			$options[CURLOPT_SSL_VERIFYPEER] = false;
			$options[CURLOPT_SSL_VERIFYHOST] = false;
        }
        
		if ($this->cookies_path) {
			$options[CURLOPT_COOKIEFILE] = $this->cookies_path;
			$options[CURLOPT_COOKIEJAR] = $this->cookies_path;
		}
        
        switch ($method) {
        	case 'POST':
        		$options[CURLOPT_POST] = true;
        		$options[CURLOPT_POSTFIELDS] = $vars;
        		break;
       		
       		case 'GET':
       		default:
       			$options[CURLOPT_HTTPGET] = true;
       			break;
        }
		return $options;
	}
	
	private function _httpParseHeaders($header) {
        $retVal = array();
        $fields = explode("\r\n", preg_replace('/\x0D\x0A[\x09\x20]+/', ' ', $header));
        foreach($fields as $field) {
            if(preg_match('/([^:]+): (.+)/m', $field, $match)) {
                $match[1] = preg_replace_callback('/(?<=^|[\x09\x20\x2D])./', create_function('$matches', 'return strtoupper($matches[0]);'), strtolower(trim($match[1])));
				
				 if(isset($retVal[$match[1]])) {
                    $retVal[$match[1]] = array($retVal[$match[1]], $match[2]);
                } else {
                    $retVal[$match[1]] = trim($match[2]);
                }
            }
        }
        return $retVal;
    }
}
?>