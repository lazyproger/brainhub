<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');

class core extends vars
{

	function __construct()
    {
		// Получаем IP и UA данные пользователя
        $this->getUserData();

		// Авторизация пользователей
        $this->authorizeUser();
		
        // TODO: разобраться с временем
        static::$TIME = time();

		//Подключаем роутер
		new router;
    }

	// Получаем IP и UA данные пользователя
	private function getUserData() {
		if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $this->ipValid($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            	static::$USER_IP = ip2long($_SERVER['HTTP_X_FORWARDED_FOR']);
     	} elseif ($_SERVER['REMOTE_ADDR']) {
            	static::$USER_IP = ip2long($_SERVER['REMOTE_ADDR']);
        }else {
        	die('Неизвестный IP адрес');
        }

        if (!isset($_SESSION['userAgent'])) {
        	$_SESSION['userAgent'] = userAgent::get();
		}

        static::$USER_AGENT = $_SESSION['userAgent'];
	}

	// Авторизация пользователей
	private function authorizeUser() {

        $id = false;
        $password = false;
        $rights = 0;

        if(isset($_SESSION['password'], $_SESSION['user_id'])) {
        	$id = $_SESSION['user_id'];
        	$password = $_SESSION['password'];
        } elseif(isset($_COOKIE['user_id'], $_COOKIE['password'])) {
        	$id = abs(intval(base64_decode($_COOKIE['user_id'])));
			$password = $_COOKIE['password'];
			$_SESSION['user_id'] = $id;
		    $_SESSION['password'] = $password;
		}

		if($id && $password) {
        	$data = DB::PDO()->query("SELECT * FROM `" . DB_PREFIX . "_users_profile` WHERE `id` = '$id' AND `password` = '$password'")->fetch();

			if($data){
				static::$USER = $data;
				// $USER['settings'] = json_decode($USER['settings'], true);
				// static::$USER_SET = $data['settings'] ? unserialize($data['settings']) : array();
				$rights = $data['rights'];
				
				if(!isset($_SESSION['userData'])) {
                    $_SESSION['userData'] = [
                        'uid'        => $data['id'],
                        'name'       => $data['login'],
                        'pic_square' => 'https://placehold.it/50&text=' . ucfirst(mb_substr($data['login'], 0, 1)),
                        'rights'     => $data['rights']
                    ]; 
                }

				// Фиксируем онлайн
                DB::PDO()->query("
                	UPDATE `" . DB_PREFIX . "_users_profile` SET
                	`browser` = '" . static::$USER_AGENT . "',
                    `ip` = '" . static::$USER_IP . "',
                    `online` = '" . time() . "'
                    WHERE `id` = '$id'
                ");

			} else {
				static::userUnset();
			}
		}

		//TODO тут запилить настройки
		if(empty(static::$USER_SET)){
			static::$USER_SET = static::defaultSettings();
		}

        // Получаем права пользователя
        static::$RIGHTS = $this->getRight($rights);

	}

   	//Проверка IP
    private function ipValid($ip = ''){
        if (empty($ip))
            return false;
        $d = explode('.', $ip);
        for ($x = 0; $x < 4; $x++)
            if (!is_numeric($d[$x]) || ($d[$x] < 0) || ($d[$x] > 255))
                return false;
        return $ip;
    }
	
	private function getRight(int $right = 0){
		$data = DB::PDO()->query("SELECT `rights` FROM `system_groups` WHERE `rang` = '{$right}'")->fetchColumn();
		return json_decode($data, true);
	}
}