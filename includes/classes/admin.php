<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');

class Admin extends vars
{
	
	/*
		Уведомления
	*/
	//Добавляем объявление
	/*
		type
		success - зеленый
		info - синий
		warning - желтый
		danger - красный
	*/
	public static function addNotice($text, $type = 'success'){
		$_SESSION['notice_text'] = $text;
		$_SESSION['notice_type'] = $type;
		$_SESSION['notice_view'] = false;
	}
	
	//Удаляем
	public static function deleteNotice(){
		unset($_SESSION['notice_text']);
		unset($_SESSION['notice_type']);
		unset($_SESSION['notice_view']);
	}
	
	//Показываем и сразу удаляем
	public static function notice(){
		if(isset($_SESSION['notice_text'])){
			echo Template::loadTpl('notice', 'HTML');
			self::deleteNotice();
		}
	}
}