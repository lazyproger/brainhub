<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');

abstract class vars
{

	// TODO: разобрать этот хламы
	public static $AJAX = array();
	public static $ALIAS = false;
	public static $SEO = array();
    public static $QUERY = array();
    public static $QUERY_COUNT = 0;
    public static $MODULES_INFO = array();
	public static $MODULE = false;
	public static $MODULE_FILE = false;
	public static $MODULE_SET = false;
	public static $DATA = array();
	public static $PATH = false;
	public static $ROUTER = array();
	public static $USER = array();
	public static $USER_SET = array();
	public static $PAGE = 1;
	public static $PLUGIN = true;
    public static $START = 0;
    public static $USER_IP = false;
    public static $USER_AGENT = false;
    public static $RIGHTS = array();
    public static $USER_TPL = 'default'; // TODO: сделать выбор темы оформления
    public static $CONFIG = array();
    public static $TIME = 0;

	// Уничтожаем данные авторизации юзера
    public static function userUnset() {
		setcookie('user_id', '', time() + 3600 * 24 * 365, '/');
		setcookie('password', '', time() + 3600 * 24 * 365, '/');
		setcookie('typeAuth', '', time() + 3600 * 24 * 365, '/');
		unset($_SESSION['user_id']);
		unset($_SESSION['password']);
		unset($_SESSION['typeAuth']);
		session_destroy();
	}

    // Настройки по умолчанию
	public static function defaultSettings(){

		return array(
			'totalPost' => 10
		);

	}

}