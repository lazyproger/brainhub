<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');

class Vk {

    const METHOD_URL_VK = 'https://api.vk.com/method/wall.post?'; //����� �������� ���������
    const ID_DEV = 5599850; //ID ���������� � ��
    const TOKEN = '10081fcf6c640cb94dcf63d25932cc2432a82f6dbe49756e591697c8c187ca2d4355518686186a2c584ad'; //ID ���������� � ��
    const URL = 'https://sportday.today/'; //URL

	public static function updatePost($article){
		$settings = json_decode($article['settings'], true);
		$method = 'https://api.vk.com/method/wall.edit?';		
		$category = Functions::getCategory($article['category']);
		
		if($settings['vk_id'] > 0){
			$fields['prevText'] = $settings['anonce'];

			$vkposter_id = '-123765957';
			$vkposter_friends_only = 0;
			$vkposter_from_group = 1;
			$vkposter_signed = 0;
			$vkposter_userid = 19459572;
			$vkposter_token = self::TOKEN;
			$url = 'https://sportday.today';
	
			$text_clear = (!empty($fields['prevText']) ? "\n\n ".$fields['prevText']."\n" : "\n");

			$argument = array(
				"owner_id" => $vkposter_id,
				"post_id" => $settings['vk_id'],
				"from_group" => $vkposter_from_group,
				"friends_only" => $vkposter_friends_only,
				"signed" => $vkposter_signed,
				"message" => $text_clear,
				"attachments" => $url.'/'.$category['alias'].'/'.$article['alias'].'/',
				"mark_as_ads" => 0
			);
			
			$out = self::sentRequesVK($method, $argument);
			
		}else{
			return false;
		}
	}
	
	
    public static function setVkWall($article, $category) {
		$fields['prevText'] = $article['settings']['anonce'];

        $vkposter_id = '-123765957';
        $vkposter_friends_only = 0;
        $vkposter_from_group = 1;
        $vkposter_signed = 0;
        $vkposter_userid = 19459572;
        $vkposter_token = self::TOKEN;

        $text_clear = (!empty($fields['prevText']) ? "\n\n ".$fields['prevText']."\n" : "\n");

        $argument = array(
            "owner_id" => $vkposter_id,
            "from_group" => $vkposter_from_group,
            "friends_only" => $vkposter_friends_only,
            "signed" => $vkposter_signed,
            "message" => $text_clear,
            "attachments" => URL.$category['alias'].'/'.$article['alias'].'/'
        );

		$out = $this->sentRequesVK(self::METHOD_URL_VK, $argument);
		return $out;
    }

    /**
     * ������� � ������� ���������
     * @param strint $method ������ ����� ��, � ��� � ������ ?
     * @param array $arg ������ ���������� ��� �������� �������
     */
    public static function sentRequesVK($method, $arg) {
        $vkposter_token = self::TOKEN; //����� ����������
        $arg['access_token'] = $vkposter_token;
        $query = http_build_query($arg);
        $url = $method . $query;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_PROXY, '');
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 12);
        $curlinfo = curl_exec($curl); //��������� �������
        $response = curl_getinfo($curl); //���������� � �������
        curl_close($curl);
        return $curlinfo;
    }

}
