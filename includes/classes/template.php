<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');


class Template
{

	private static $instance = null;
	private static $params = array();
    public static $pluginTpl = 'html';
    public static $PLUGIN_SET = array();

	// Инициализация

	public static function getInstance(){

        if (is_null(self::$instance)) {
            self::$instance = new Template;
        }
        return self::$instance;
    }

   // Подключение плагина

	private static function getPlugin($plugin = array()) {

		$content = 'Error: the plug-in isn\'t found';

		if(is_file(ROOTPATH . 'plugins/' . $plugin[1] . '/core.php')) {
			// Параметры плагина
         	$plugin[2] = trim($plugin[2]);
          	if($plugin[2]) {
                preg_replace_callback('/([\dA-z_-]{1,25})\=\"(.*?)\"/', 'self::getParams', $plugin[2]);
                if(self::$params) {
                    static::$PLUGIN_SET = self::$params;
					self::$params = array();
     			}
			}
			
			$PLUGIN_SET = static::$PLUGIN_SET;
			
   			// Подключаем плагин
   			include(ROOTPATH . 'plugins/' . $plugin[1] . '/core.php');

			// Получаем tpl файл плагина
			if(is_file(ROOTPATH . 'templates/' . Vars::$USER_TPL . '/html/plugins/' . $plugin[1] . '/' . self::$pluginTpl . '.php'))  {
		        include(ROOTPATH . 'templates/' . Vars::$USER_TPL . '/html/plugins/' . $plugin[1] . '/' . self::$pluginTpl . '.php');
		    } elseif(is_file(ROOTPATH . 'plugins/' . $plugin[1] . '/' . self::$pluginTpl . '.php')) {
		        include(ROOTPATH . 'plugins/' . $plugin[1] . '/' . self::$pluginTpl . '.php');
		    } else {
		        echo 'Файл шаблона для плагина <b>' . $plugin[1] . '</b> не найден';
		    }

			$content = ob_get_contents();
   			ob_clean();
		}

        return $content;
	}

   // Получаем параметры плагина

	private static function getParams($params = array()) {
    	$params[1] = trim($params[1]);

        if($params[1]) {
        	self::$params[$params[1]] = trim($params[2]);
        }
	}

	// Формируем путь к файлу модуля

	private static function getModuleFile($module = false, $path = false) {
		// Формируем директорию шаблона
		if($path == 'HTML') {
			$path = '';
		}elseif($path){
			$path = is_string($path) ? 'modules/' . $path . '/' : '';
		} else {
			$path = 'modules/' . Vars::$PATH . '/';
		}

        // Получаем адрес шаблона
		if(is_file(ROOTPATH . 'templates/' . Vars::$USER_TPL . '/html/' . $path . $module . '.php'))  {
        	$moduleTpl = ROOTPATH . 'templates/' . Vars::$USER_TPL . '/html/' . $path . $module . '.php';
       	} elseif(is_file(ROOTPATH . $path . '_tpl/' . $module . '.php') && $path) {
       		$moduleTpl = ROOTPATH . $path . '_tpl/' . $module . '.php';
       	} else {
        	$moduleTpl = false;
        }

		return $moduleTpl;
	}

	// Загрузка дополнительных шаблонов
	public static function loadTpl($module, $path = false, $data = false) {
		$tpl = self::$instance;

		// Получаем tpl файл модуля
		$moduleTpl = self::getModuleFile($module, $path);
        
        // Проверка на наличие файла шаблона
     	if(!$moduleTpl) {
     		return false;
      	} else {
			// Подключаем файла шаблона
			include($moduleTpl);      	 
      	}
    }

	// Подключение файла шаблона
	public static function includeTpl($_module, $_tplfile = 'main', $settings = array()){

		$tpl = self::$instance;
		
		// Добавляем возможность модели игнорировать настройки выбора темы и задать свою
		if(isset($tpl->template)) Vars::$USER_TPL = $tpl->template;
		
		// Получаем tpl файл модуля
        $moduleTpl = self::getModuleFile($_module);

		// Проверка на наличие файлов шаблона
     	if(!is_file(ROOTPATH . 'templates/' . Vars::$USER_TPL . '/' . $_tplfile . '.php') || !$moduleTpl) {
     		return false;
      	}

        //Включаем буфер
        ob_start();

			// Получаем содержимое модуля
			require_once($moduleTpl);
			$content = ob_get_contents();
			ob_clean();

		   
			require_once(ROOTPATH . 'templates/' . Vars::$USER_TPL . '/' . $_tplfile . '.php');

			$content = ob_get_contents();
		ob_clean();
		
		if(Vars::$PLUGIN){
			// Обрабатываем плагины
			$content = preg_replace_callback('/<plugin="([\dA-z_-]{3,25})"(.*?)>/', 'self::getPlugin', $content);
		}
		//Заголовки
		header('Cache-Control: public');
		header('Cache-control: max-age=30');
		header('Content-type: text/html; charset=UTF-8');

		echo $content;
    	
	}
	
	//Выдаем 404 ошибку
	public static function error404(){
		$tpl = self::$instance;
		//Меняем тему
		$tpl->template = '404';
		//Выводим шаблон
		$tpl->includeTpl('404');
	}
	

}