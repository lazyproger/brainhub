<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');

class userAgent {

	public static function get() {
	    $user_agent = htmlentities(trim($_SERVER['HTTP_USER_AGENT']), ENT_QUOTES);
	    if (stripos($user_agent, 'Avant Browser') !== false)
	        return 'Avant Browser';
	    elseif (stripos($user_agent, 'Acoo Browser') !== false)
	        return 'Acoo Browser';
	    elseif (stripos($user_agent, 'MyIE2') !== false)
	        return 'MyIE2';
	    elseif (preg_match('|Iron/([0-9a-z\.]*)|i', $user_agent, $array))
	        return 'SRWare Iron ' . self::substrUa($array[1], '.', 0, 2);
	    elseif (preg_match('|Chrome/([0-9a-z\.]*)|i', $user_agent, $array))
	        return 'Chrome ' . self::substrUa($array[1], '.', 0, 3);
	    elseif (preg_match('#(Maxthon|NetCaptor)( [0-9a-z\.]*)?#i', $user_agent, $array))
	        return $array[1] . $array[2];
	    elseif (stripos($user_agent, 'Safari') !== false && preg_match('|Version/([0-9]{1,2}.[0-9]{1,2})|i', $user_agent, $array))
	        return 'Safari ' . self::substrUa($array[1], '.', 0, 3);
	    elseif (preg_match('#(NetFront|K-Meleon|Netscape|Galeon|Epiphany|Konqueror|Safari|Opera Mini|Opera Mobile)/([0-9a-z\.]*)#i', $user_agent, $array))
	        return $array[1] . ' ' . self::substrUa($array[2], '.', 0, 2, 2);
	    elseif (stripos($user_agent, 'Opera') !== false && preg_match('|Version/([0-9]{1,2}.[0-9]{1,2})|i', $user_agent, $array))
	        return 'Opera ' . $array[1];
	    elseif (preg_match('|Opera[/ ]([0-9a-z\.]*)|i', $user_agent, $array))
	        return 'Opera ' . self::substrUa($array[1], '.', 0, 2);
	    elseif (preg_match('|Orca/([ 0-9a-z\.]*)|i', $user_agent, $array))
	        return 'Orca ' . self::substrUa($array[1], '.', 0, 2);
	    elseif (preg_match('#(SeaMonkey|Firefox|GranParadiso|Minefield|Shiretoko)/([0-9a-z\.]*)#i', $user_agent, $array))
	        return $array[1] . ' ' . self::substrUa($array[2], '.', 0, 3);
	    elseif (preg_match('|rv:([0-9a-z\.]*)|i', $user_agent, $array) && strpos($user_agent, 'Mozilla/') !== false)
	        return 'Mozilla ' . self::substrUa($array[1], '.', 0, 2);
	    elseif (preg_match('|Lynx/([0-9a-z\.]*)|i', $user_agent, $array))
	        return 'Lynx ' . self::substrUa($array[1], '.', 0, 2);
	    elseif (preg_match('|MSIE ([0-9a-z\.]*)|i', $user_agent, $array))
	        return 'IE ' . self::substrUa($array[1], '.', 0, 2);
	    else {
	        $user_agent = preg_replace('|http://|i', '', $user_agent);
	        $user_agent = strtok($user_agent, '( ');
	        $user_agent = self::substrUa(substr($user_agent, 0, 22), '.', 0, 2);
	        return ($user_agent ? $user_agent : 'Unknown');
	    }
	}

	private static function substrUa($string, $chr, $pos, $len = null, $val = false) {
		$array = explode($chr, $string);
	    if($val && $array[1])
	    	$array[1] = mb_substr($array[1], 0, $val);
		return implode($chr, array_slice($array, $pos, $len));
	}

}