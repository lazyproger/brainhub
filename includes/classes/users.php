<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');

class users {

	public static function  main($user = array(), $text = '', $link = '', $avatar = '50', $ip = 1){
		$tpl = Template::getInstance();

		$tpl->data = $user;

		// Подключаем шаблона
		$tpl->loadTpl('user', true);
	}

	//Получаем юзера по IDшнику
	public static function getById(int $id){
		$user = DB::PDO()->query("SELECT * FROM `brhb_users_profile` WHERE `id` = '".$id."'")->fetch();

		if(!empty($user)){
			return $user;
		}
	}

	//Функция ограничения доступа
	public static function access($param, $module = ''){
		if(empty($module)) $module = Vars::$MODULE;
		if(isset(Vars::$RIGHTS[$module][$param])){
			if(Vars::$RIGHTS[$module][$param] == 1){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	//Отдать ссылку на аватарку
	public static function foto_take($id){
		return '/files/avatar/'.$id.'.jpg';
	}

	//Проверяем наличие аватарки
	public static function foto_exist($id = 0){
		if($id == 0) $id = Vars::$USER['id'];

		if(file_exists((ROOTPATH.'/files/avatar/'.$id.'.jpg')))
			return true;
		else
			return false;
	}


}
