<?php

defined('_IN_BRHB_RU') or die('Error: restricted access');

class Notice
{

	//Добавление уведомления
	public static function add($user_id, $type, $who = 0, $other_id = 0){
			
		$recent_notification = DB::PDO()->query('SELECT COUNT(*) FROM `' . DB_PREFIX . '_notice` WHERE 
			`user_id`="' . $user_id . '" AND 
			`type`="'.$type.'" AND 
			`other_id`="'.$other_id.'" AND
			`read`="1"
		')->fetchColumn();
		
		//Если похожая запись ненайдена
		if(!$recent_notification){
			
			//Добавляем уведомление
			$STH = DB::PDO()->prepare('
			INSERT INTO `' . DB_PREFIX . '_notice` SET
				`user_id` = ?,
				`type`      	= ?,
				`who`      	= ?,
				`other_id`      	= ?,
				`time` = ?
			');
			
			$STH->execute(array(
				$user_id,
				$type,
				$who,
				$other_id,
				time()
			));
			
			//Обновляем счетчик
			DB::PDO()->query("UPDATE `" . DB_PREFIX . "_users_profile` SET `notice`= `notice` + 1 WHERE `id` = " . $user_id);
			
			$STH = NULL;
			
		}
	}
}