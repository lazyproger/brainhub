<?php 
// namespace Csspress;

/**
* CSSPress class
*
* This class provide functionalities to compress css.
*
* @author Towry Wang <tovvry@gmail.com>
*/
class csspress
{
	const T_START  	= '<';
	const T_END     = '>';
	const WS_REGEX  = '%([\s;:,\{\}\*#\.]){1}%';

	private $incomment;
	private $intag;

	private $buffer;
	
	/**
	 * Constructor
	 *
	 * @access public
	 */
	public function __construct()
	{
		# code...
	}

	/**
	 * Compress css
	 *
	 * @param string
	 * @access public
	 */
	public function press($text) 
	{
		$this->reset();

		$len = strlen($text);
		for ($i = 0; $i < $len; $i++) {
			$char = substr($text, $i, 1);

			if ($char == self::T_END) {
				$this->intag = false;
				$this->buffer .= $char;
				continue;
			}

			if ($this->intag) {
				$this->buffer .= $char;
				continue;
			}

			/** trim whitespace */
			if ($char == ' ') {
				$next = substr($text, $i+1, 1);
				$pre = substr($text, $i-1, 1);

				if (preg_match(self::WS_REGEX, $next, $matched)) {
					if ($matched[1] != '#' && $matched[1] != '.') {
						continue;
					} elseif ($pre == '}' || $pre == '{') {
						continue;
					}
				}

				if (preg_match(self::WS_REGEX, $pre)) {
					continue;
				}
			}

			if ($this->incomment) {
				if ($char != '/') continue;
				elseif (substr($text, $i-1, 1) == '*') {
					$this->incomment = false;
					continue;
				}
				continue;
			}

			/** erase comments */
			if ($char == '/') {
				$next = substr($text, $i+1, 1);
				$pre = substr($text, $i-1, 1);

				if ($next == '*') {
					$this->incomment = true;
					continue;
				}

				if ($pre == '*') {
					$this->incomment = false;
					continue;
				}
				$this->buffer .= $char;
				continue;
			}

			if ($char == self::T_START) {
				while ($char !== self::T_END) {
					$this->buffer .= $char;
					$i++;
					$char = substr($text, $i, 1);
				}

				$this->buffer .= $char;
				continue;
			}

			/** erase `\n`, '\t' and `\r` */
			if ($char == "\n" || $char == "\t" || $char == "\r") continue;

			$this->buffer .= $char;
		}

		return $this->buffer;
	}

	/**
	 * Reset all flags
	 *
	 * @access private
	 */
	private function reset()
	{
		$this->incomment = false;
		$this->intag 	 = false;
		$this->buffer    = '';
	}
}
